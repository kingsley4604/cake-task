<?php

require __DIR__ . '/../helpers.php';

$scenarioNumber = 1; // 1 or 2

list ($x, $y, $k, $a, $b) =  $scenarios[$scenarioNumber];

$result = solution($x, $y, $k, $a, $b);

var_dump($result);
