<?php

require __DIR__ . '/Models/Cake.php';
require __DIR__ . '/services/Chef.php';

function solution($x, $y, $k, array $a, array $b)
{
    $chef = new Chef();

    $piecesOfCake = $chef->cutCake($chef->bakeCake($x, $y), $a, $b);

    rsort($piecesOfCake);

    return $piecesOfCake[$k - 1];
}

$scenarios = [
    [],
    [6, 7, 8, [1, 3], [1, 5]],
    [35, 38, 15, [1, 3, 6, 11, 24, 26, 32], [1, 5, 8, 13, 16, 22, 26]],
];
