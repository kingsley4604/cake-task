<?php

class Chef
{
    /**
     * Bake a Cake.
     *
     * @param $x
     * @param $y
     * @return Cake
     */
    public function bakeCake($x, $y)
    {
        // Bake a new cake with the given [x, y] parameters.
        return new Cake($x, $y);
    }

    /**
     * Cut the cake by the given $a & $b coordinates.
     *
     * @param Cake $cake
     * @param array $a
     * @param array $b
     * @return array
     */
    public function cutCake(Cake $cake, array $a, array $b)
    {
        // A list of all cut pieces.
        $pieces = [];

        $weightPieces = self::determineCutXCoordinates($cake, $a);
        $heightPieces = self::determineCutYCoordinates($cake, $b);

        foreach ($weightPieces as $weightPiece) {
            foreach ($heightPieces as $heightPiece) {
                // Determine a cut piece multiplying X with Y.
                $pieces[] = $weightPiece * $heightPiece;
            }
        }

        return $pieces;
    }

    /**
     * Determine cut X coordinates.
     *
     * @param Cake $cake
     * @param $a
     * @return array
     */
    protected function determineCutXCoordinates(Cake $cake, $a)
    {
        $xCoordinates = [];

        $latestPieceCoordinate = 0;

        foreach ($a as $coordinate) {
            $xCoordinates[] = $coordinate - $latestPieceCoordinate;
            $latestPieceCoordinate = $coordinate;
        }

        if ($cake->getX() > $latestPieceCoordinate) {
            $xCoordinates[] = $cake->getX() - $latestPieceCoordinate;
        }

        return $xCoordinates;
    }

    /**
     * Determine cut Y coordinates.
     *
     * @param Cake $cake
     * @param $b
     * @return array
     */
    protected function determineCutYCoordinates(Cake $cake, $b)
    {
        $yCoordinates = [];

        $latestPieceCoordinate = 0;

        foreach ($b as $coordinate) {
            $yCoordinates[] = $coordinate - $latestPieceCoordinate;
            $latestPieceCoordinate = $coordinate;
        }

        if ($cake->getY() > $latestPieceCoordinate) {
            $yCoordinates[] = $cake->getY() - $latestPieceCoordinate;
        }

        return $yCoordinates;
    }
}
